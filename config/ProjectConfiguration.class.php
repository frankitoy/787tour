<?php

if ($_SERVER['SF_ENVIRONMENT'] == 'test')
{
   require_once '/usr/share/php5/symfony-1.4/lib/autoload/sfCoreAutoload.class.php';
}
else
{
  require_once '/usr/share/php5/symfony-1.4/lib/autoload/sfCoreAutoload.class.php';
}



sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfDoctrinePlugin','sfImageTransformPlugin');
  }

  static public function getApplicationConfiguration($application, $environment = null, $debug = null, $rootDir = null, sfEventDispatcher $dispatcher = null)
  {
    if (null === $environment)
    {
      if (!empty($_SERVER['SF_ENVIRONMENT']))
      {
        $environment = $_SERVER['SF_ENVIRONMENT'];
      }
      else
      {
        throw new sfException('Could not determine environment.');
      }
    }

    if (null === $debug)
    {
      $debug = ($environment == 'niks');
    }
    
    return parent::getApplicationConfiguration($application, $environment, $debug, $rootDir, $dispatcher);
  }
}
