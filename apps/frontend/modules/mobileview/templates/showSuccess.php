
<div class="showphoto">
    <a class="largelink" href="<?php echo $largesource ?>"><img src="<?php echo $thumbsource ?>" alt="Thanks for visiting our new 777" /></a>
</div>

    <p><a class="largelink" href="<?php echo $largesource ?>">View and download the large image</a></p>

<div class="png_bg" id="social_menu">
    <h5>Share</h5>
    <a target="_blank" href="http://www.facebook.com/share.php" id="share_facebook_generic"><img alt="Share on Facebook" src="/images/social-button-facebook.png"></a>
</div>

<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<div id="fb-root"></div>
  <script>
    $(document).ready(function(){
        $('#share_facebook_generic').bind('click', function(){
            shareFacebook( "http://www.thenew777tour.co.nz" + $('.largelink').attr('href') );
            return false;
        });

    });

    window.fbAsyncInit = function() {
      FB.init({
        "apiKey":"5eb9b27965ce2a4f21134e9e28e22037",
        "status":true, "cookie":true, "xfbml":true
      });
    };

    (function() {
      var e = document.createElement('script');
      e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
      e.async = true;
      document.getElementById('fb-root').appendChild(e);
    }());

    function shareFacebook(imageUrl)
    {
        
        if (imageUrl)
        {
            // Post the users image
            FB.ui({
                method: 'feed',
                name: 'I got up close and personal',
                link: window.location,
                picture: imageUrl,
                description: 'I got up close and personal with Air New Zealand\'s brand new Boeing 777'
            });
        } else {
            // Generic share this site.
            FB.ui({
                method: 'feed',
                name: 'I got up close and personal',
                link: window.location,
                picture: 'http://www.thenew777tour.co.nz/photocache/photologo.png',
                description: 'I got up close and personal with Air New Zealand\'s brand new Boeing 777'
            });
        }

        return false;
    }




  </script>