<?php

/**
 * mobileview actions.
 *
 * @package    airnzevents
 * @subpackage mobileview
 * @author     Malcolm Fell
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mobileviewActions extends sfActions
{
    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */

    public function executeIndex(sfWebRequest $request)
    {

        $this->form = $this->retrivePhotoForm();
    }

    public function executeSubmit(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod('post'));

        $this->redirect('mobileview/show?photocode='.$request->getParameter('photocode'));
    }

    public function executeShow(sfWebRequest $request)
    {




        $id = $request->getParameter('photocode');

        $ch = curl_init("http://airnzevents.satellitenz.com/api.php?id={$id}");
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $photofile = curl_exec($ch);
        curl_close($ch);

        if($photofile != ""){
            $remote_path = 'http://airnzevents.satellitenz.com/upload/' . $photofile;
            $local_path = sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR . 'photocache' . DIRECTORY_SEPARATOR;
            $local_tmp = $local_path . 'tmp' . DIRECTORY_SEPARATOR . $photofile;
            if( !file_exists( $local_path . DIRECTORY_SEPARATOR . 'large' . DIRECTORY_SEPARATOR . $photofile ) )
            {
                //create the large photo and put the logo on it
                $this->save_image($remote_path, $local_tmp);


                $img = new sfImage($local_tmp, 'image/jpg' );


                $img->thumbnail(1000,1000,"scale");

                $img->overlay(new sfImage($local_path.'photologo.png','image/png'), array(25,25));

                $img->saveAs($local_path.'large'.DIRECTORY_SEPARATOR.$photofile);


                unset($img);

                $img = new sfImage($local_path.'large'.DIRECTORY_SEPARATOR.$photofile, 'image/jpg');
                $img->thumbnail(390,390,"scale");
                $img->saveAs($local_path.'thumb'.DIRECTORY_SEPARATOR.$photofile);


            }



            $this->thumbsource = '/photocache/thumb/' . $photofile;
            $this->largesource = '/photocache/large/' . $photofile;
            $this->photocode = $id;
        } else {
            $this->forward404("Invalid image code");
        }


    }

    public function retrivePhotoForm()
    {
        $form = new sfForm();

        $form->setWidgets(array(
            'photocode' => new sfWidgetFormInputText(array(),array('class' => 'photocodeinput'))
        ));

        $form->setValidator('photocode' , new sfValidatorNumber() );


        return $form;

    }

    function save_image($img,$fullpath){
        $ch = curl_init ($img);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $rawdata=curl_exec($ch);
        curl_close ($ch);
        if(file_exists($fullpath)){
            unlink($fullpath);
        }
        $fp = fopen($fullpath,'x');
        fwrite($fp, $rawdata);
        fclose($fp);
    }

}
