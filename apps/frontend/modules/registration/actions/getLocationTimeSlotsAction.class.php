<?php

class getLocationTimeSlotsAction extends sfAction
{
  public function execute($request)
  {
    $location = $this->getRoute()->getObject();

    $this->timeSlots = $location->getAvailableTimeSlots();
  }
}