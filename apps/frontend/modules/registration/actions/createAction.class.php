<?php

class createAction extends sfAction
{
  public function execute($request)
  {
      
    // login first
      
    if ($request->getParameter("loc") == 1) {
        if ($this->getRequest()->getCookie('reg') != 'airnz_akl') {
            $this->redirect('@login');
        }
    } elseif ($request->getParameter("loc") == 2) {
     if ($this->getRequest()->getCookie('reg') != 'airnz_ch') {
            $this->redirect('@ch_login');
        } 
        
        
        // Required for CH
        //$this->widgetSchema['your_age']->setOption('label', 'Date of Birth *');
        //$this->validatorSchema['your_age'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter date of birth'));
    }
      
    if ($this->getUser()->hasFlash('registration_id'))
    {
      $this->registration = Doctrine::getTable('Registration')->find($this->getUser()->getFlash('registration_id'));

      return 'Response';
    }

    $registration = null;
    
    $this->form = new RegistrationForm($registration);
    
    // auckland specific
    $this->time_slots = Doctrine::getTable('TimeSlot')->findByLocationId($request->getParameter("loc"));
    
    $this->locations = Doctrine::getTable('Location')->findAll();

    if ($request->isMethod('post') && !$this->itIsAfterFivePmFriday11thNov)
    {
        
      $formValues = $request->getParameter($this->form->getName());

      if (!$formValues['ticket_count_type'])
      {
        $formValues['additional_names'] = array();
      }

      if (false !== ($registration = $this->form->bindAndSave($formValues)))
      {
        $this->registration = $this->form->getObject();
        
        $obj = $this->form->getObject();
        $pm = (array) $obj;

        $this->sendNotificationEmail($this->registration);

        $this->getUser()->setFlash('registration_id', $this->registration->id);
        
        if($request->getParameter("loc") == 1) {
            $this->redirect('@registration');
        } elseif ($request->getParameter("loc") == 2) {
            $this->redirect('@ch_registration');
        }
      } else {
        $this->scrollintoview = true;
      }
    }
    
    return 'Form_' . $request->getParameter("loc");
  }

  public function sendNotificationEmail(Registration $registration)
  {
    $from = array(sfConfig::get('app_email_from_address') => sfConfig::get('app_email_from_name'));
    $to = array($registration->email => $registration->name);
    $subject = "Boeing 787-8 Dreamliner Open Day";
    $body = $this->getPartial('registration/email', array('registration' => $registration));
    
    $message = $this->getMailer()->composeAndSend($from, $to, $subject, $body);
  }
}
