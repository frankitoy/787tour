<br/><br/>

<div class="form_row form_row_time_slot">

  <label for="registration_location_id" style="display:block; width:auto; float:none;"><strong>Choose your time/place</strong></label>

  <div class="form_field_container">

    <?php echo $form['location_id']->render(); ?>
    <?php echo $form['time_slot_id']->render(); ?>

    <?php if ($form['location_id']->hasError() || $form['time_slot_id']->hasError()): ?>
      <div class="form_field_error">
        <?php echo $form['location_id']->getError() ?>
        <?php echo $form['time_slot_id']->getError() ?>
      </div>
    <?php endif; ?>

  </div>

</div>
