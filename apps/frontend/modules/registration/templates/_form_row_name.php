<div class="form_row form_row_name">

  <label for="registration_name" style="float:none; display:block; width:auto;"><strong>Full name:</strong> (as per your photo ID)</label>

  <div class="form_field_container">

    <?php echo $field->render(); ?>

    <?php if ($field->hasError()): ?>
      <div class="form_field_error">
        <?php echo $field->getError() ?>
      </div>
    <?php endif; ?>

  </div>

</div>

<div class="form_row"><br /></div>