<img class="register_title" src="<?php echo image_path('/images/title.gif') ?>" />

<br class="clearfix" /><br />

Air New Zealand is thrilled to announce that we will be welcoming the Boeing 787 Dreamliner back to our shores in May, with a special visit planned to Christchurch. <br /><br />

Air New Zealand will be hosting the Boeing 787-8 test aircraft at its Christchurch engineering base and we’d like to invite you to register to come and see this brand new aircraft designed with fuel efficiency and passenger comfort in mind. <br /><br />

Air New Zealand is the launch customer for the larger 787-9 variant, which will have a greater range capability and seating capacity than the 787-8. Once the 787-9 goes into service for Air New Zealand in 2014 it will change how New Zealanders fly. <br /><br />

We’re holding a special event in Christchurch, on Wednesday 30 May, for a select group of people and would like to invite you to this exclusive opportunity. Register now if you’d like to be one of the lucky few to get the chance to see this 787-8 aircraft which has now been fitted out with its much talked about, stunning interior and will give guests a glimpse of what to expect when Air New Zealand receives its highly-anticipated new fleet. <br /><br />

<a href="<?php echo url_for('@ch_information') ?>">Click here for the location information.</a><br /><br />

You can register to attend, with a guest, for one of the Boeing 787 sessions by filling in the form below. <br /><br />

<div class="register_box">

<?php if (1): ?>

<p>Sorry, registrations have now closed.</p>

<!--<p><a href="<?php echo url_for('gallery') ?>">Click here</a> to view photos from the 787-8 'Dreamliner' open day.</p>-->

<?php else: ?>

    Register Now<br /><br />
    
    <form action="<?php echo url_for('@ch_registration') ?>" method="post" id="registration_form" name="registration_form">
    

        <?php if ($form->hasGlobalErrors()): die('error'); ?>
        
            <ul class="form_errors">
            <?php foreach($form->getGlobalErrors() as $fieldName => $error): ?>
                <li><?php echo $error ?></li>
            <?php endforeach; ?>
            </ul>
        
        <?php endif; ?>

        <fieldset>

            <div class="form_row form_row_name">
                <label for="registration_name"><strong>Full name *</strong>: (as per your photo ID)</label>

                <div class="form_field_container">
                    <?php echo $form['name']->render(); ?>
                    <?php include_partial('registration/field_error', array('field' => $form['name'])) ?>
                </div>
            </div>
            
            <div class="form_row form_row_your_age">
                <label for="registration_your_age"><strong>Date of Birth</strong>:</label>

                <div class="form_field_container">
                    <?php echo $form['your_age']->render(); ?>
                    <?php include_partial('registration/field_error', array('field' => $form['your_age'])) ?>
                </div>
            </div>
            
        </fieldset>
        
        <br />

        <fieldset>

            <strong>Personal details</strong>

            <?php
                $fields = array('address', 'mobile', 'air_points_number', 'email', 'confirm_email');
                foreach ($fields as $fieldName):
            ?>

            <div class="form_row form_row<?php echo $form[$fieldName]->getName() ?> form_row_inline clearfix">

            <?php if ($fieldName == 'address') {echo '<label for="registration_address">Your home address *</label>'; } else {echo $form[$fieldName]->renderLabel();} ?>
                <div class="form_field_container">
                  <?php echo $form[$fieldName]->render(); ?>
                  <?php include_partial('registration/field_error', array('field' => $form[$fieldName])) ?>
                </div>

            </div>
            <?php endforeach; ?>

        </fieldset>
        
        <br />
        
        <fieldset>
            <div class="form_row form_row_guest_name">
                <label for="registration_guest_name"><strong>Guest's name</strong>:</label>

                <div class="form_field_container">
                    <?php echo $form['guest_name']->render(); ?>
                    <?php include_partial('registration/field_error', array('field' => $form['guest_name'])) ?>
                </div>
            </div>
            
            <div class="form_row form_row_guest_date_of_birth">
                <label for="registration_agency_name"><strong>Guest's date of birth</strong>:</label>

                <div class="form_field_container">
                    <?php echo $form['agency_name']->render(); ?>
                </div>
            </div>
        </fieldset>
        
        <br />

        <fieldset>

            <div class="form_row form_row_time_slot_id">
            <label>
            <strong>Session I'd like to attend</strong></label>
                
            <div class="form_field_container" id="test_<?php $form['location_id']->renderLabel(); ?>">
                <div style="visibility: hidden; height: 0; width: 0">
                    <?php $form->setDefault('location_id', '2'); echo $form['location_id']->render(); ?>
                </div>
                
                <?php //echo $form['time_slot_id']->render(); ?>

                <select id="registration_time_slot_id" name="registration[time_slot_id]">
                    <option selected="selected" value="">Please Select</option>
                    <?php foreach($time_slots as $time): ?>
                        <?php $start_ = explode(':', $time->start) ?>
                        <?php $end_ = explode(':', $time->end) ?>
                    <option value="<?php echo $time->id ?>">
                            <?php echo date("h:i A",  mktime($start_[0], $start_[1], $start_[1]));  ?> - <?php echo date("h:i A",  mktime($end_[0], $end_[1], $end_[1]));  ?></option>
                    <?php endforeach; ?>
                </select>

                <?php if ($form['location_id']->hasError() || $form['time_slot_id']->hasError()): ?>
                    <div class="form_field_error">
                <?php echo $form['location_id']->getError() ?>
                <?php echo $form['time_slot_id']->getError() ?>
                </div>
                <?php endif; ?>
            </div>
                
<!--            <br /><span style="color: red">Registrations for this event have now closed</span>-->
            </div>
            
            <br />

            <div class="form_row form_row_additional_notes">
            <label><strong>Additional notes:</strong><br /> Please state below if you have a physical disability (or any other requirements) and will need to be assisted by our crew onto the aircraft, please include full details:</label>
            <div class="form_field_container">
                <?php echo $form['additional_notes']->render(); ?>
            </div>
            </div>

        </fieldset>
        
        <br />

        <fieldset>

            <div class="form_row form_row_terms_and_conditions">
            <?php echo $form['terms_and_conditions']->render(); ?>
                <label for="registration_terms_and_conditions">I have read and agree to the</label> <a href="<?php echo url_for('@ch_terms_and_conditions') ?>" target="_blank">Terms &amp; Conditions</a>
            <div class="form_field_container">
                <?php include_partial('registration/field_error', array('field' => $form['terms_and_conditions'])) ?>
            </div>
            </div>

        </fieldset>
        
        <br />

        <fieldset>
            <div class="form_row form_row_submit clearfix">
            <div class="form_field_container">
                <input type="submit" value="Register" />
            </div>
            </div>
        </fieldset>

        <span style="font-size: 12px">Fields marked with an * are required</span>

        <?php $form->renderHiddenFields() ?>

    </form>
    
    <?php if($scrollintoview): ?>
        <script>
            document.getElementById('registration_form').scrollIntoView();
        </script>
    <?php endif; ?>

<?php endif; ?>

</div>
