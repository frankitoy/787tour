<img class="register_title" src="<?php echo image_path('/images/title.gif') ?>" />
<br /><br />

<strong style="font-size: 15px;">Terms and Conditions</strong><br /><br />

<ul>
    <li>Tickets once booked are non-transferable</li>
    <li>Event will go ahead rain or shine</li>
    <li>Flat, closed-toe shoes must be worn</li>
    <li>No smoking on buses or onboard the aircraft</li>
    <li>No sharp objects, food or beverages may be taken onboard the aircraft</li>
    <li>
        Air New Zealand reserves the right to refuse entry to 787 tour if, in the exercise of our reasonable discretion, we decide or establish any of the following:
        <ul>
            <li>such action is necessary for reasons of safety;</li>
            <li>your conduct, age or mental or physical state including your impairment from alcohol or drugs, is such as to require special assistance, cause discomfort or make yourself objectionable to other passengers or involve any hazard or risk to yourself or to other persons or to property;</li>
            <li>such action is necessary because you have failed to observe any of our instructions including non compliance with these Conditions in relation to carriage of dangerous or prohibited goods;</li>
            <li>you do not appear to have a valid e-ticket</li>
            <li>the Ticket you present has been acquired unlawfully or has been procured from an entity other than us; has been reported as being lost or stolen or is a counterfeit; </li>
            <li>you cannot prove that you are the person named in the e-ticket;</li>
            <li>we have notified you in writing that we would not, after the date of such notice, carry you on our flights or those of Our Operators. </li>
            <li>you are wearing or otherwise displaying attire which we deem offensive or believe will cause discomfort to other passengers and you have refused to remove such offensive items.</li>
        </ul>
    </li>
</ul>