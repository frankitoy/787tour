<div class="form_row registration_additional_names">

  <div class="form_field_container">
    <div class="array_rows small_fields">
      <?php echo $form['additional_names']->render(); ?>
    </div>

    <?php if ($form['additional_names']->hasError()): ?>
      <div class="form_field_error">
        <?php echo $form['additional_names']->getError() ?>
      </div>
    <?php endif; ?>

  </div>

</div>
