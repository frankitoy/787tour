<div class="form_row form_row_ticket_count">

  <label style="float:none; display:block; width:auto;"><strong>I am also registering for:</strong> Up to four people</label>

  <div class="form_field_container">

    <div class="ticket_count_type">
      <?php echo $form['ticket_count_type']->render(); ?>

      <?php echo $form['ticket_count_select']->render() ?>

      others
    </div>

  </div>

</div>