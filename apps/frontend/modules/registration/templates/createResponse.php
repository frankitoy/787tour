<img class="register_title" src="<?php echo image_path('/images/title.gif') ?>" />
<br /><br />

<p>Thank you for registering for the Boeing 787 tour in <?php echo $registration->TimeSlot->Location ?> on <?php echo date('l j F', $registration->TimeSlot->timestamp) ?>, 2012</p>
 
<p>You have been registered for the <?php echo date('g:ia', $registration->TimeSlot->timestamp) ?> session.</p>

<p>For Aviation Security reasons, please ensure that you bring an official form of photo identification (NZ driver’s license, passport or HANZ ID) with you.</p>
 
<p>You will need to wear flat, closed-toe shoes and please arrive 15 minutes prior to your session.</p>

<?php if ($registration->TimeSlot->Location == "Auckland"): ?>

<p>We look forward to seeing you on Thursday 31 May.</p>

<p><strong>Car parking and registration venue: </strong></p>

<p>The car park and bus transport for this event will be located at 1 Geoffrey Roberts Rd – Air NZ Property Building, Auckland Airport.</p>

<p>Once you have parked, please make your way to the registration tent to check-in and await your bus onto the engineering base. </p>

<p>Buses will leave at 0945, please ensure you arrive by 0930 to register and board your bus.</p>
 
<p>If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact <a href="mailto: airnzevents@airnz.co.nz">airnzevents@airnz.co.nz</a> or call 0800 737 000</p> 

<p>Return to the <a href="<?php echo url_for('@registration') ?>">registration form</a>.</p>

<?php elseif($registration->TimeSlot->Location == "Christchurch"): ?>

<p>We look forward to seeing you on Wednesday 30 May.</p>

<p><strong>Car parking and registration venue: </strong> Christchurch Technical Operations, 125 Orchard Rd, Christchurch Airport</p>

<p>Once you have parked, please follow signage to the registration tent to check-in. Please arrive 15 minutes prior to your session start time.</p>
 
<p>If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact <a href="mailto: airnzevents@airnz.co.nz">airnzevents@airnz.co.nz</a> or call 0800 737 000</p>

<p>Return to the <a href="<?php echo url_for('@ch_registration') ?>">registration form</a>.</p>

<?php endif; ?>