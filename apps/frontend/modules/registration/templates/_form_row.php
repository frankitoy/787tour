<?php if (!$field->isHidden()): ?>

  <div class="form_row">

    <?php echo $field->renderLabel(); ?>

    <div class="form_field_container">

      <?php echo $field->render(); ?>

      <?php if ($field->hasError()): ?>
        <div class="form_field_error">
          <?php echo $field->getError() ?>
        </div>
      <?php endif; ?>

    </div>

  </div>

<?php else: ?>

  <?php echo $field->render(); ?>

<?php endif; ?>