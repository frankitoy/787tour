<img class="register_title" src="<?php echo image_path('/images/title.gif') ?>" />

<p><strong style="font-size: 15px;">Information</strong></p>

<p><strong>Car parking and registration venue:</strong></p>

<p><iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Geoffrey+Roberts+Rd,+Mangere,+Auckland&amp;aq=&amp;sll=-37.001966,174.794626&amp;sspn=0.003256,0.006459&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Geoffrey+Roberts+Rd,+Auckland+Airport,+Auckland+2022,+Auckland,+New+Zealand&amp;ll=-37.002814,174.795169&amp;spn=0.003256,0.006459&amp;z=16&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Geoffrey+Roberts+Rd,+Mangere,+Auckland&amp;aq=&amp;sll=-37.001966,174.794626&amp;sspn=0.003256,0.006459&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Geoffrey+Roberts+Rd,+Auckland+Airport,+Auckland+2022,+Auckland,+New+Zealand&amp;ll=-37.002814,174.795169&amp;spn=0.003256,0.006459&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small></p>
 
<p>The car park and bus transport for this event will be located at 1 Geoffrey Roberts Rd – Air NZ Property Building, Auckland Airport. </p>

<p>Once you have parked, please make your way to the registration tent to check-in and await your shuttle onto the engineering base. </p>

<p>Buses will leave at 0920, please ensure you arrive by 0900 to register and board your bus.</p>

<p>If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact <a href="mailto: airnzevents@airnz.co.nz">airnzevents@airnz.co.nz</a> or call 0800 737 000.</p>

<p>Return to the <a href="<?php echo url_for('@registration') ?>">registration form</a>.</p>
