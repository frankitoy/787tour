<?php
    $timeslot_start = explode(":",$registration->TimeSlot->start);
    $timebase_start = mktime($timeslot_start[0], $timeslot_start[1], $timeslot_start[2], 0, 0, 0); 
    
    $timeslot_end = explode(":",$registration->TimeSlot->end);
    $timebase_end = mktime($timeslot_end[0], $timeslot_end[1], $timeslot_end[2], 0, 0, 0); 
    
    $datebase = explode("-", $registration->TimeSlot->Location->eventdate);
    $date_display = mktime(0, 0, 0,$datebase[1], $datebase[2], $datebase[0]); 
?>

Thank you for registering for the Boeing 787 tour in <?php echo $registration->TimeSlot->Location ?> on <?php echo date('l j F', $registration->TimeSlot->timestamp) ?>, 2012.


You registered for the <?php echo date('g:ia', $registration->TimeSlot->timestamp) ?> session. This confirmation email will serve as your e-ticket, please bring a print-out of this email with you.


For Aviation Security reasons, please ensure that you bring an official form of photo identification (NZ driver's license, passport or HANZ ID) with you.


You will need to wear flat, closed-toe shoes and please arrive ten minutes prior to your session.


<?php if($registration->TimeSlot->Location == "Auckland"): ?>

We look forward to seeing you on Thursday 31 May.


Car parking and registration venue:


The car park and bus transport for this event will be located at 1 Geoffrey Roberts Rd – Air NZ Property Building, Auckland Airport. 


Once you have parked, please follow signage to the registration tent to check-in. Please arrive 15 minutes prior to your session start time.


If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact airnzevents@airnz.co.nz or call 0800 737 000.


<?php elseif($registration->TimeSlot->Location == "Christchurch"): ?>

We look forward to seeing you on Wednesday 30 May.


Car parking and registration venue: Christchurch Technical Operations, 125 Orchard Rd, Christchurch Airport


Once you have parked, please follow signage to the registration tent to check-in. Please arrive 15 minutes prior to your session start time.


If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact airnzevents@airnz.co.nz or call 0800 737 000.


<?php endif;?>
