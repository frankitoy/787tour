<img class="register_title" src="<?php echo image_path('/images/title.gif') ?>" />

<p><strong style="font-size: 15px;">Information</strong></p>

<p><strong>Christchurch: </strong> Christchurch Technical Operations, 125 Orchard Rd, Christchurch Airport</p>

<p><strong>Car parking and registration venue:</strong></p>

<p><iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Christchurch%2BCity%2FHarewood%2F125%2BOrchard%2BRoad%2F&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=51.310143,78.486328&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=125+Orchard+Rd,+Harewood,+Christchurch+8051,+Canterbury,+New+Zealand&amp;z=14&amp;ll=-43.481222,172.552054&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Christchurch%2BCity%2FHarewood%2F125%2BOrchard%2BRoad%2F&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=51.310143,78.486328&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=125+Orchard+Rd,+Harewood,+Christchurch+8051,+Canterbury,+New+Zealand&amp;z=14&amp;ll=-43.481222,172.552054" style="color:#0000FF;text-align:left">View Larger Map</a></small></p>
 
<p>Once you have parked, please follow signage to the registration tent to check-in. Please arrive 15 minutes prior to your session start time.</p>

<p>If you have a query or need to cancel your registration, please note once booked session times cannot be changed, please contact <a href="mailto: airnzevents@airnz.co.nz">airnzevents@airnz.co.nz</a> or call 0800 737 000.</p>

<p>Return to the <a href="<?php echo url_for('@ch_registration') ?>">registration form</a>.</p>
