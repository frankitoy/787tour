<?php

$timeSlotsArray = array();

foreach ($timeSlots as $timeSlot)
{
  $timeSlotsArray[] = array(
    'id'   => $timeSlot->id,
    'name' => (string) $timeSlot
  );
}

echo json_encode($timeSlotsArray);