<div class="form_row form_row_terms">

  <label>&nbsp;</label>

  <div class="form_field_container">

    <?php echo $field->render(); ?>
    <label for="registration_terms_and_conditions" style="width:auto; float:none;">I have read &amp; agree to the <a href="<?php echo url_for('@terms_and_conditions') ?>">Terms &amp; Conditions</a></label>

    <?php if ($field->hasError()): ?>
      <div class="form_field_error">
        <?php echo $field->getError() ?>
      </div>
    <?php endif; ?>

  </div>

</div>
