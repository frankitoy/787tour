<?php

class registrationAction extends sfAction
{
  public function promptLogin()
  {
    header('WWW-Authenticate: Basic realm="Air NZ Events"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'This is a restricted area.';
    exit;
  }

  public function preExecute()
  {
    if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
      $this->promptLogin();
    }
    elseif (!($_SERVER['PHP_AUTH_USER'] == 'airnzevents' && $_SERVER['PHP_AUTH_PW'] == 'r8nuzAd9'))
    {
      $this->promptLogin();
    }
  }

  public function execute($request)
  {
    sfConfig::set('sf_web_debug', false);
    $this->setLayout(false);
    
    $response = $this->getResponse();
    $response->setContentType('text/csv');
    $response->setHttpHeader('Content-disposition', sprintf('attachment;filename=airnz_events_registrations_%s.csv', date('Y-m-d') . "-" . rand(5,99)));

    $this->registrations = Doctrine::getTable('Registration')->createQuery('r')
      ->leftJoin('r.TimeSlot ts')
      ->leftJoin('ts.Location')
      ->where('ts.location_id = ?', $request->getParameter("loc"))
      ->orderBy('r.created_at DESC')
      ->execute();
    
    $this->setTemplate("registration_" . $request->getParameter("loc"));
  }
}