<?php

$registrations = $registrations->getRawValue();

if (false === ($filename = tempnam(sys_get_temp_dir(), 'airnz')))
{
  throw new sfException('Could not generate unique CSV filename.');
}

if (false === ($handle = fopen($filename, 'w+')))
{
  throw new sfException('Could not open CSV for writing.');
}

fputcsv($handle, array(
  'Name',
  'Address',
  'Email',
  'Mobile',
  'Airpoints number',
  'Number of tickets',
  'Time',
  'City',
  'Registration Date',
  'Additional notes from registrant'
));

foreach ($registrations as $registration)
{
    
  $additional_names = array();
  
  // create additional names 
  foreach ($registration->additional_names as $k => $v) {
      $additional_names[] = $v . "(" . $registration->additional_names_ages[$k] . ")";
  }
  
  fputcsv($handle, array(
    $registration->name,
    $registration->address,
    $registration->email,
    $registration->mobile,
    $registration->air_points_number,
    $registration->ticket_count,
    (string) $registration->TimeSlot,
    $registration->TimeSlot->Location->name,
    $registration->created_at,
    $registration->additional_notes
  ));
}

rewind($handle);

echo fread($handle, filesize($filename));

fclose($handle);

unlink($filename);
