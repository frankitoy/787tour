<?php use_javascript('jquery_lightbox.js') ?>
<?php use_stylesheet('jquery_lightbox.css') ?>
<?php
$results = glob('images/gallery/auckland-nov-14/thumb/*.JPG');
sort($results);
?>
<br/>
<div style="margin-bottom: 10px;">
    <span style="float: left" class="bluelink">November 14 2011, Auckland</span>
    <div style="clear: both"></div>
</div>

<center>
<?php foreach($results as $key => $thumbnails): ?>
    <?php 
        $get_id = explode(".",$thumbnails); 
        $container_id = $get_id[0] . "_container";
        $image_id = $get_id[0] . "_image";
        $downloadAnchor = htmlspecialchars('<a href="'.url_for(array('sf_route' => 'gallery_download', 'filename' => basename($thumbnails))).'">Download</a>');
    ?>
    <div class="tn_container" id="<?php echo $container_id ?>">
        <a rel="lightbox" href="/images/gallery/auckland-nov-14/full/<?php echo basename($thumbnails) ?>" title="<?php echo $downloadAnchor ?>">
            <img src="/images/gallery/auckland-nov-14/thumb/<?php echo basename($thumbnails) ?>" id="<?php echo $image_id ?>" title="<?php echo basename($thumbnails) ?>" />
        </a>
    </div>
    
<?php endforeach; ?>
</center>

<br class="clear" />

<script type="text/javascript">
$(function() {
    $("a[rel*=lightbox]").lightBox();
});
</script>
