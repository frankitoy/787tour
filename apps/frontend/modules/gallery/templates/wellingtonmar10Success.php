<?php

if (!$current_page) {
    $offset = 0;
    $limit = 101;
} elseif ($current_page == 2) {
    $offset = 102;
    $limit = 203;
} elseif ($current_page == 3) {
    $offset = 203;
    $limit = 304;
} 
    
$results = array();
$handler = opendir('images/gallery/new_wellington/mar10_tn/');

while ($file = readdir($handler)) {
    if ($file != "." && $file != ".." && $file != ".svn" && $file != "Thumbs.db") {
        $results[] = $file;
    }
}
closedir($handler);
sort($results);
?>

<p style="width: 654px" class="smalltext">For further details on Air New Zealand's new aircraft interiors, seats and in-flight menus, events and entertainment, you can visit <a href="http://www.futuretakingflight.com" target="blank">www.futuretakingflight.com</a>  </p>
 
<p style="width: 654px" class="smalltext">To book your seat to Los Angeles or London on Air New Zealand's new Boeing 777-300ER, please visit <a href="http://www.airnz.co.nz/">www.airnz.co.nz</a> for airfare and holiday package details. The 777-300ER will be in service from 1 April 2011.</p>

<div style="width: 649px; margin-bottom: 10px;">
    <span style="float: left" class="bluelink">March 10 2011, Wellington</span>
    <span style="float: right" class="smalltext">
        Page: 
            <a class="page_number <?php if(!$current_page) echo "page_number_selected" ?>" href="/gallery/wellington/mar10">1</a>
            <a class="page_number <?php if($current_page == 2) echo "page_number_selected" ?>" href="/gallery/wellington/mar10/2">2</a>
            <a class="page_number <?php if($current_page == 3) echo "page_number_selected" ?>" href="/gallery/wellington/mar10/3">3</a>
    </span>
    <div style="clear: both"></div>
</div>

<center>
<?php foreach($results as $key => $thumbnails): ?>
    <?php if ($key >= $offset AND $key <= $limit): ?>
    <?php 
        $get_id = explode(".",$thumbnails); 
        $container_id = $get_id[0] . "_container";
        $image_id = $get_id[0] . "_image";
    ?>
    <div class="tn_container" id="<?php echo $container_id ?>">
        <a rel="lightbox" href="/images/gallery/new_wellington/mar10/<?php echo $thumbnails ?>">
            <img src="/images/gallery/new_wellington/mar10_tn/<?php echo $thumbnails ?>" id="<?php echo $image_id ?>" />
        </a>
    </div>
    
    <?php endif; ?>
<?php endforeach; ?>
</center>

<br class="clear" />
<div style="width: 649px; margin-bottom: 10px;">
    <span style="float: right" class="smalltext">
        Page: 
            <a class="page_number <?php if(!$current_page) echo "page_number_selected" ?>" href="/gallery/wellington/mar10">1</a>
            <a class="page_number <?php if($current_page == 2) echo "page_number_selected" ?>" href="/gallery/wellington/mar10/2">2</a>
            <a class="page_number <?php if($current_page == 3) echo "page_number_selected" ?>" href="/gallery/wellington/mar10/3">3</a>
    </span>
    <div style="clear: both"></div>
</div>

<script type="text/javascript">
$(function() {
    $("a[rel*=lightbox]").lightBox();
});
</script>
