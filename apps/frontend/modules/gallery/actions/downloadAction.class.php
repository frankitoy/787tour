<?php

class downloadAction extends sfAction
{
	public function execute($request)
	{
		$path = 'images/gallery/auckland-nov-14/full';
		$filename = basename(trim($request->getParameter('filename'), '/\\')); // basename and trim prevent people downloading system files outside of the webroot
		$absoluteFilename = realpath($path.DIRECTORY_SEPARATOR.$filename);
		
		// Ensure noone frigging with the filename to download
		$this->forward404If(realpath(sfConfig::get('sf_web_dir').DIRECTORY_SEPARATOR.$path) !== dirname($absoluteFilename));
		$this->forward404Unless(file_exists($absoluteFilename));
		
		$response = $this->getResponse();
		$response->setHttpHeader('Content-type', 'image/jpeg');
    $response->setHttpHeader('Content-disposition', 'attachment; filename="'.$filename.'"');
    $response->setContent(file_get_contents($absoluteFilename));
    
		return sfView::NONE;
	}
}