<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/images/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body class="clearfix">

    <div id="container" class="clearfix">

      <div id="content">
            <div class="header_logo">
                <a href="/"><img src="<?php echo image_path('/images/logo_header.jpg') ?>" alt="Air New Zealand 787 Tour"/></a>
            </div>
          
            <div class="main_content">
                <div class="section">
                    <div class="box_top"></div>
                    <div class="article">
                        <?php echo $sf_content ?>
                    </div>
                </div>
            </div>
          
<!--          <div class="footer_notes">
              &copy; 2011 Air New Zealand Ltd. <a href="mailto:airnzevents@airnz.co.nz">Click here</a> to contact us about this promotion. 
          </div>--><br />
      </div>
    </div>
  </body>
</html>
