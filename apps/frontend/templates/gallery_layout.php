<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body class="clearfix">

    <div id="container-gallery" class="clearfix">

      <img id="logo" src="<?php echo image_path('/images/logo-air-nz.png') ?>" alt="Air New Zealand 777-300 ZK-OKM"/>
      <img id="heading" src="<?php echo image_path('/images/hd-get-up-close-and-personal.png') ?>" alt="Get up close and personal"/>
      <img id="intro" src="<?php echo image_path('/images/hd-air-nz-brand-new-boeing.png') ?>" alt="with Air New Zealand's brand new Boeing 777-300ER"/>

      <div id="content-gallery">
        <?php echo $sf_content ?>
        <br style="clear: both"/>
        <br /><br /><br />
        
        <center>
            <div class="smallertext">
                <a href="#" class="whitelink">Back to top</a> <br />
                <!--Other Galleries: <a href="#" class="bluelink">Auckland</a>&nbsp;&nbsp; <a href="#" class="bluelink">Wellington</a>&nbsp;&nbsp; <a href="#" class="bluelink">Christchurch</a>-->
            </div>
        </center>
        <br /><br /><br />
      </div>
 
    </div>
    
  </body>
</html>
