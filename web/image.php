<?php


function clean($str)
{
  return htmlspecialchars($str,ENT_QUOTES);
}


ob_start();

echo "headers\n";
print_r(apache_request_headers());

echo "\n\npost\n";
print_r($_POST);

echo "\n\nfiles\n";
print_r($_FILES);

//mysql db
$host = 'localhost';
$db= 'airnzevents_eventpix';
$user= 'eventpix';
$pw= 'St6qAw8W';

//file upload and move
$target_path  = "./upload/";
$filename = clean($_POST['activity']."_".$_POST['device']."_".strftime("%s").".jpg");
$target_path = $target_path . $filename;
$result = array();
$result['fileupload'] = move_uploaded_file($_FILES['file']['tmp_name'], $target_path);


try {
  $dbh = new PDO("mysql:host={$host};dbname={$db}", $user, $pw);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
  
  $dbh->exec("INSERT INTO photos (activity_id,device_id,filename) VALUES ('".clean($_POST['activity'])."','".clean($_POST['device'])."','".$filename."');");
  $id = $dbh->lastInsertId();
  $dbh = null;
  
  $result['code'] = $id;
  
} catch(PDOException $e) {
  echo $e->getMessage();
}
file_put_contents('android/'.date('r'), ob_get_clean());


if (2 == $_POST['activity'] && 10 == $_POST['device'])
{
  // Send email to picasa album to go straight on site http://airnzeverestteam2011.blogspot.com/
  send_to_picasa('mikejallsop.airnz2011@picasaweb.com', 'Air NZ Base Camp Trip Photos', $target_path);
}

function send_to_picasa($secretEmail, $albumName, $filename)
{
  if (!file_exists($filename)) return;

  require_once 'Swift-4.0.6/lib/swift_required.php';

  $transport = Swift_SmtpTransport::newInstance('localhost', 25);

  $mailer = Swift_Mailer::newInstance($transport);

  $message = Swift_Message::newInstance($albumName)
    ->setFrom(array('no-reply@airnzevents.satellitenz.com' => 'AirNZ Android App'))
    ->setTo(array($secretEmail))
    ->attach(Swift_Attachment::fromPath($filename, 'image/jpeg'));

  return $mailer->send($message);
}

echo json_encode($result);
