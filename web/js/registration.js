(function($){
var $form = $('form'),

    $ticketCountSelect = $('#registration_ticket_count_select'),
    $additionalNamesContainer = $('.form_row_additional_names'),
    $ticketTypeSelect = $('input[name="registration[ticket_count_type]"]'),
    rowTemplate = 
        '<div class="form_row form_row_inline clearfix">' + 
            '<label>Person {x}\'s name<br>Age</label><div class="form_field_container">' + 
                '<div class="round_field">'+ 
                    '<div>' + 
                        '<input type="text" name="registration[additional_names][]" value="{name}" />' +
                    '</div>' + 
                '</div>' +
                '<div class="form_field_container"><div class="round_field">'+ 
                    '<div>' + 
                        '<input type="text" name="registration[additional_names_ages][]" value="" />' +
                    '</div>' + 
                '</div></div>' +
        '</div>' + 
        '</div>',
    

    $timeSlotSelect = $('#registration_time_slot_id'),
    $locationSelect = $('#registration_location_id');

$form.bind('reload_time_slots', function(){
  var locationId = $locationSelect.val(),
      timeSlotId = $timeSlotSelect.val();

  $('option', $timeSlotSelect).remove();

  $('#location_1')[(1 == locationId) ? 'show' : 'hide']();
  $('#location_2')[(2 == locationId) ? 'show' : 'hide']();
  $('#location_3')[(3 == locationId) ? 'show' : 'hide']();
  $('#location_4')[(4 == locationId) ? 'show' : 'hide']();
  $('#location_5')[(5 == locationId) ? 'show' : 'hide']();

  if (locationId)
  {
    $timeSlotSelect.append($('<option/>').attr('value', '').html('Loading...'));

    $.ajax({
      url: '/location-time-slots/' + locationId,
      type: 'get',
      dataType: 'json',
      success: function(response)
      {
        $('option', $timeSlotSelect).remove();
        $timeSlotSelect.append($('<option/>').attr('value', '').html('Select a time'));

        for (var x=0, l=response.length; x<l; x++)
        {
          var option = $('<option/>').attr('value', response[x].id).html(response[x].name);

          if (timeSlotId == response[x].id) option.attr('selected', 'selected');

          $timeSlotSelect.append(option);
        }
      }
    });
  }
  else
  {
    $timeSlotSelect.append($('<option/>').attr('value', '').html('Please select a location'));
  }
});

$form.bind('toggle_guest_names', function(){
  var selectedTicketCountType = $ticketTypeSelect.filter(':checked').val(),
      selectedTicketCount = $ticketCountSelect.val(),
      additionalNamesCount = $('.form_row', $additionalNamesContainer).length;

  if (0 == selectedTicketCountType)
  {
    $('.form_row', $additionalNamesContainer).remove();

    $additionalNamesContainer.hide();

    return;
  }
  else
  {
    $additionalNamesContainer.show();
  }

  if (selectedTicketCount < additionalNamesCount)
  {
    $('.form_row:gt('+(selectedTicketCount - 1)+')', $additionalNamesContainer).remove();
  }

  for (var x=additionalNamesCount; x<selectedTicketCount; x++)
  {
    $additionalNamesContainer.append(rowTemplate.replace('{x}', x + 2).replace('{name}', ''));
  }
});

$locationSelect.bind('change', function(){
  $form.trigger('reload_time_slots');
});

$ticketCountSelect.bind('change', function(){
  $ticketTypeSelect[1].checked = true;
  $form.trigger('toggle_guest_names');
});

$ticketTypeSelect.bind('change', function(){
  $form.trigger('toggle_guest_names');
}).bind('click', function(){
  $form.trigger('toggle_guest_names');
});

$(function(){
  $form.trigger('reload_time_slots');
  $form.trigger('toggle_guest_names');
});

$('input[type="text"]').wrap($('<div><div/></div>').addClass('round_field'));

if (0 < ($('.form_field_error').length + $('.form_errors').length))
{
  location.href = '#registration_form';
}

})(jQuery);
