<ul><?php

$path = '.';
$files = array();

if ($handle = opendir($path)) {
    echo "Directory handle: $handle\n";
    echo "Files:\n";

    /* This is the correct way to loop over the directory. */
    while (false !== ($file = readdir($handle))) {
        $files[filemtime($file)] = $file;
    }

    closedir($handle);
}

ksort($files, SORT_NUMERIC);

foreach ($files as $file)
{
    echo "<li><a href=\"$file\">$file</a></li>";
}

?></ul>
