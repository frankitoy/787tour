<?php

/**
 * Registration form.
 *
 * @package    airnzevents
 * @subpackage form
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class RegistrationForm extends BaseRegistrationForm
{
  public function configure()
  {
    // Name
    $this->widgetSchema['name']->setOption('label', 'Name (as per ID)');
    $this->validatorSchema['name'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter your name.'));

    // Ticket count
    $this->widgetSchema->moveField('ticket_count', 'after', 'name');
    $this->widgetSchema['ticket_count_type'] = new sfWidgetFormSelectRadio(array('choices' => array('Just Me', 'Me and')));
    $this->validatorSchema['ticket_count_type'] = new sfValidatorPass(array('required' => false));
    $this->widgetSchema['ticket_count_select'] = new sfWidgetFormSelect(array('choices' => $this->getChoicesForTicketCountSelect()));
    $this->validatorSchema['ticket_count_select'] = new sfValidatorPass(array('required' => false));
    
    // Address
    $this->widgetSchema['address']->setOption('label', 'Your home address *');
    $this->validatorSchema['address'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter your home address.'));
    
    // Agency Naeme
    $this->widgetSchema['agency_name']->setOption('label', 'Agency/Brand Name *');
    $this->validatorSchema['agency_name'] = new sfValidatorString(array('required' => false));
    
    // Guest Name
    $this->widgetSchema['guest_name']->setOption('label', 'Guest Name');
    $this->validatorSchema['guest_name'] = new sfValidatorString(array('required' => false));
    
    $this->widgetSchema['your_age']->setOption('label', 'Date of Birth *');
    $this->validatorSchema['your_age'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter date of birth'));

    // Email
    $this->widgetSchema['email']->setOption('label', 'Your email address *');
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => true), array('required' => 'Please enter your email address.', 'invalid' => 'Please enter a valid email address.'));
    
    // Mobile
    $this->widgetSchema['mobile']->setOption('label', 'Mobile number *');
    $this->validatorSchema['mobile'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter mobile number'));

    // Air points number
    $this->widgetSchema['air_points_number']->setOption('label', 'Air NZ Airpoints number (if you have one)');

    // Confirm email
    $this->widgetSchema['confirm_email'] = new sfWidgetFormInputText(array('label' => 'Confirm email address *'));
    $this->validatorSchema['confirm_email'] = new sfValidatorString(array('required' => true), array('required' => '&nbsp;'));
    $this->widgetSchema->moveField('confirm_email', 'after', 'email');

    // Additional names
    $this->widgetSchema->moveField('additional_names', 'after', 'ticket_count');
    $this->widgetSchema['additional_names'] = new ArrayWidget;
    $this->widgetSchema['additional_names_ages'] = new ArrayWidget;
    
    // Location
    $this->widgetSchema['location_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'Location', 'add_empty' => 'Location'));
    $this->validatorSchema['location_id'] = new sfValidatorDoctrineChoice(array('model' => 'Location', 'required' => true), array('required' => 'Please select.', 'invalid' => 'Please select a valid location.'));

    // Time slot
    $this->widgetSchema['time_slot_id']->setOption('add_empty', 'Please select');
    $this->validatorSchema['time_slot_id']->setOption('required', true);
    $this->validatorSchema['time_slot_id']->setMessage('required', 'Please select a time slot.');
    $this->validatorSchema['time_slot_id']->setMessage('invalid', 'Please select a valid time slot.');

    // Terms & Conditions
    $this->widgetSchema['terms_and_conditions'] = new sfWidgetFormInputCheckbox;
    $this->validatorSchema['terms_and_conditions'] = new sfValidatorBoolean(array('required' => true), array('required' => 'You must read and agree to the terms and conditions.'));

    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
      new sfValidatorSchemaCompare('email', sfValidatorSchemaCompare::EQUAL, 'confirm_email', array(), array('invalid' => 'Please confirm your email address correctly.')),
      new sfValidatorCallback(array('callback' => array($this, 'validateRequireAdditionalNames'))),
      new sfValidatorCallback(array('callback' => array($this, 'validatePreventOverBooking')))
    )));

    $this->setDefaults(array(
      'ticket_count_type' => 0
    ));

    unset(
      $this['created_at'],
      $this['updated_at']
    );
  }

  public function validateRequireAdditionalNames($validator, $values)
  {
    $wantsToInviteFriends = $values['ticket_count_type'];
    $additionalCount = $values['ticket_count_select'];
    $additionalNames = $values['additional_names'];

    $additional_name_count = 0;
    foreach ($values['additional_names'] as $name_value) {
        if ($name_value) {
            $additional_name_count += 1;
        }
    }
    
    //if ($wantsToInviteFriends && $additionalCount != count($additionalNames))
    if ($wantsToInviteFriends && $additionalCount != $additional_name_count)
    {
        echo "adding error code";
      $error = new sfValidatorError($validator, 'Please enter the names of all additional people');

      throw new sfValidatorErrorSchema($validator, array('additional_names' => $error));
    }
    
    return $values;
  }

  public function validatePreventOverBooking($validator, $values)
  {
    if (!empty($values['time_slot_id']))
    {
      $timeSlot = Doctrine::getTable('TimeSlot')->find($values['time_slot_id']);
      $wantsToInviteFriends = $values['ticket_count_type'];
      $additionalCount = $values['ticket_count_select'];
      $guestName = $values['guest_name'];

      $ticketCount = 1;

      if ($wantsToInviteFriends) $ticketCount += $additionalCount;

      if (!empty($guestName)) $ticketCount++;
      
      if (!$timeSlot->canAccommodateNewBookings($ticketCount, $values['location_id']))
      {
        if ($timeSlot->isFull($values['location_id']))
        {
          $error = new sfValidatorError($validator, 'Unfortunately your time slot has just booked out. Please select another time.');
        }
        else
        {
          //$error = new sfValidatorError($validator, sprintf('This time slot only has %d ticket(s) available.', $timeSlot->getAvailableTicketCount($values['location_id'])));
          $error = new sfValidatorError($validator, 'Registrations have closed for this session');
        }

        throw new sfValidatorErrorSchema($validator, array('time_slot_id' => $error));
      }
    }

    return $values;
  }

  public function getChoicesForTicketCountSelect()
  {
    $choices = range(1, 4);

    return array_combine($choices, $choices);
  }
}
