<?php

/**
 * Registration form base class.
 *
 * @method Registration getObject() Returns the current form's model object
 *
 * @package    airnzevents
 * @subpackage form
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseRegistrationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInputText(),
      'your_age'              => new sfWidgetFormInputText(),
      'address'               => new sfWidgetFormInputText(),
      'email'                 => new sfWidgetFormInputText(),
      'mobile'                => new sfWidgetFormInputText(),
      'additional_notes'      => new sfWidgetFormInputText(),
      'agency_name'           => new sfWidgetFormInputText(),
      'guest_name'           => new sfWidgetFormInputText(),
      'air_points_number'     => new sfWidgetFormInputText(),
      'ticket_count'          => new sfWidgetFormInputText(),
      'additional_names'      => new sfWidgetFormInputText(),
      'additional_names_ages' => new sfWidgetFormInputText(),
      'time_slot_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TimeSlot'), 'add_empty' => true)),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                  => new sfValidatorPass(array('required' => false)),
      'your_age'              => new sfValidatorInteger(array('required' => false)),
      'address'               => new sfValidatorPass(array('required' => false)),
      'email'                 => new sfValidatorPass(array('required' => false)),
      'mobile'                => new sfValidatorPass(array('required' => false)),
      'additional_notes'      => new sfValidatorPass(array('required' => false)),
      'agency_name'           => new sfValidatorPass(array('required' => false)),
      'guest_name'           => new sfValidatorPass(array('required' => false)),
      'air_points_number'     => new sfValidatorPass(array('required' => false)),
      'ticket_count'          => new sfValidatorInteger(array('required' => false)),
      'additional_names'      => new sfValidatorPass(array('required' => false)),
      'additional_names_ages' => new sfValidatorPass(array('required' => false)),
      'time_slot_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TimeSlot'), 'required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('registration[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Registration';
  }

}
