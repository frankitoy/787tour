<?php

class LocationTable extends Doctrine_Table
{
  public static function getInstance()
  {
    return Doctrine_Core::getTable('Location');
  }

  public function getAvailableTimeSlotsQuery($locationId, Doctrine_Query $query = null)
  {
    if (null === $query)
    {
      $query = Doctrine::getTable('TimeSlot')->createQuery();
    }

    $rootAlias = $query->getRootAlias();

    return $query
      ->where(sprintf('%s.location_id = ?', $rootAlias), $locationId)
      ->andWhere(sprintf('%s.allocated_count < ?', $rootAlias), sfConfig::get('app_time_slot_registration_limit', 110))
      ->orderBy(sprintf('%s.start ASC', $rootAlias));
  }

  public function getAvailableTimeSlots($locationId)
  {
    return $this->getAvailableTimeSlotsQuery($locationId)->execute();
  }

}