<?php

/**
 * Registration filter form.
 *
 * @package    airnzevents
 * @subpackage filter
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class RegistrationFormFilter extends BaseRegistrationFormFilter
{
  public function configure()
  {
  }
}
