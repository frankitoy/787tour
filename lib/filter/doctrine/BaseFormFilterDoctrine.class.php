<?php

/**
 * Project filter form base class.
 *
 * @package    airnzevents
 * @subpackage filter
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormFilterBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormFilterDoctrine extends sfFormFilterDoctrine
{
  public function setup()
  {
  }
}
