<?php

/**
 * TimeSlot filter form base class.
 *
 * @package    airnzevents
 * @subpackage filter
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTimeSlotFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'location_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Location'), 'add_empty' => true)),
      'start'           => new sfWidgetFormFilterInput(),
      'end'             => new sfWidgetFormFilterInput(),
      'allocated_count' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'location_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Location'), 'column' => 'id')),
      'start'           => new sfValidatorPass(array('required' => false)),
      'end'             => new sfValidatorPass(array('required' => false)),
      'allocated_count' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('time_slot_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TimeSlot';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'location_id'     => 'ForeignKey',
      'start'           => 'Text',
      'end'             => 'Text',
      'allocated_count' => 'Number',
    );
  }
}
