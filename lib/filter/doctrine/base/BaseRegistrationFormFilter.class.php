<?php

/**
 * Registration filter form base class.
 *
 * @package    airnzevents
 * @subpackage filter
 * @author     Malcolm Fell
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseRegistrationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                  => new sfWidgetFormFilterInput(),
      'your_age'              => new sfWidgetFormFilterInput(),
      'address'               => new sfWidgetFormFilterInput(),
      'email'                 => new sfWidgetFormFilterInput(),
      'mobile'                => new sfWidgetFormFilterInput(),
      'additional_notes'      => new sfWidgetFormFilterInput(),
      'agency_name'           => new sfWidgetFormFilterInput(),
      'guest_name'           => new sfWidgetFormFilterInput(),
      'air_points_number'     => new sfWidgetFormFilterInput(),
      'ticket_count'          => new sfWidgetFormFilterInput(),
      'additional_names'      => new sfWidgetFormFilterInput(),
      'additional_names_ages' => new sfWidgetFormFilterInput(),
      'time_slot_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TimeSlot'), 'add_empty' => true)),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'                  => new sfValidatorPass(array('required' => false)),
      'your_age'              => new sfValidatorPass(array('required' => false)),
      'address'               => new sfValidatorPass(array('required' => false)),
      'email'                 => new sfValidatorPass(array('required' => false)),
      'mobile'                => new sfValidatorPass(array('required' => false)),
      'additional_notes'      => new sfValidatorPass(array('required' => false)),
      'agency_name'           => new sfValidatorPass(array('required' => false)),
      'guest_name'           => new sfValidatorPass(array('required' => false)),
      'air_points_number'     => new sfValidatorPass(array('required' => false)),
      'ticket_count'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'additional_names'      => new sfValidatorPass(array('required' => false)),
      'additional_names_ages' => new sfValidatorPass(array('required' => false)),
      'time_slot_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TimeSlot'), 'column' => 'id')),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('registration_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Registration';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'name'                  => 'Text',
      'your_age'              => 'Text',
      'address'               => 'Text',
      'email'                 => 'Text',
      'mobile'                => 'Text',
      'additional_notes'      => 'Text',
      'agency_name'           => 'Text',
      'guest_name'           => 'Text',
      'air_points_number'     => 'Text',
      'ticket_count'          => 'Number',
      'additional_names'      => 'Text',
      'additional_names_ages' => 'Text',
      'time_slot_id'          => 'ForeignKey',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
    );
  }
}
