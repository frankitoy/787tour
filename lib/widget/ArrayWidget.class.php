<?php

class ArrayWidget extends sfWidgetFormInputText
{
  public function configure($options = array(), $attributes = array())
  {
    $this->addOption('separator', '');

    parent::configure($options, $attributes);
  }

  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    if (!is_array($value))
    {
      $value = array($value);
    }
    
    $fields = array();
    
    foreach ($value as $i => $iValue)
    {

      $fields[] = '
        <div class="form_row form_row_inline clearfix">
            <label>' . sprintf('Person %d\'s name <br >Age', $i + 2) . '</label>
                <div class="form_field_container">' . 
                    parent::render($name . '[]', $iValue, $attributes, $errors) . '
                </div>
                <div class="form_field_container">' .
                    parent::render("registration[additional_names_ages]" . '[]', "", $attributes, $errors) . '
                </div>
            </div>';
    }

    return implode($this->getOption('separator'), $fields);
  }
}